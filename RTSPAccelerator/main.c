//
//  main.c
//  R2HServer
//
//  Created by  on 11/10/24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include "MainServer.h"
#include "socket.h"

#define UNIT_TEST 1
extern void unit_test();


#ifndef __ANDROID__
int main (int argc, const char * argv[])
{
#if UNIT_TEST
    unit_test();
#else
    pthread_t a_thread;
    pthread_create(&a_thread, NULL, socket_server, NULL);

    while(1){
        char command[256];
        printf("(rtsp/close/pause/resume) > ");
        scanf("%s", command);
        printf("YOUR COMMAND - %s\n", command);
        send_internal_command(command);
    }
#endif

    return 0;
}
#endif
