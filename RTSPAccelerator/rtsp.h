//
//  rtsp.h
//  R2HServer
//
//  Created by Nat on 2016/9/29.
//
//

#ifndef rtsp_h
#define rtsp_h

#include "utility.h"
#include <stdio.h>
#include <netinet/in.h>

#define OPTIONS     "OPTIONS"
#define DESCRIBE    "DESCRIBE"
#define SETUP       "SETUP"
#define PLAY        "PLAY"
#define TEARDOWN    "TEARDOWN"
#define PAUSE       "PAUSE"
#define GET_PARAMETER "GET_PARAMETER"

#define MAGICID     0x24

#define INTERLEAVED_HEADER_LEN  4
#define RTP_HEADER_LEN          12


#define NALU_TYPE_SLICE     1
#define NALU_TYPE_DPA       2
#define NALU_TYPE_DPB       3
#define NALU_TYPE_DPC       4
#define NALU_TYPE_IDR       5
#define NALU_TYPE_SEI       6   //Supplemental Enhancement Information
#define NALU_TYPE_SPS       7   //Sequence Parameter Sets
#define NALU_TYPE_PPS       8   //Picture Parameter Sets
#define NALU_TYPE_AUD       9
#define NALU_TYPE_EOSEQ     10
#define NALU_TYPE_EOSTREAM  11
#define NALU_TYPE_FILL      12


#define FU_A                28
#define FU_B                29

#define MAX_SEGMENTED_COUNT 10240


typedef struct{
    unsigned char   magic;
    unsigned char   channel;
    unsigned short  length;
}INTERLEAVED_FRAME_HEADER;

typedef struct{
    unsigned char   flags;
    unsigned char   mark:1;
    unsigned char   type:7;
    unsigned short  seq_num;
    
    unsigned int    timestamp;
    unsigned int    syn_identifier;
    
}RTP_HEADER;

typedef struct data_segmentation_buffer{
    unsigned char   *ptr;
    unsigned short  size;
    unsigned short  copied_len;
    unsigned short  need_len;
}DATA_SEGMENTATION_BUFFER;

typedef struct segmented_data{
    unsigned char   *data_ptr_array[MAX_SEGMENTED_COUNT];
    unsigned short  data_len_array[MAX_SEGMENTED_COUNT];
    unsigned short  segmented_count;
}SEGMENTED_DATA;

typedef enum{
    E_OPTIONS=2,
    E_DESCRIBE_STATE,
    E_SETUP_AUDIO_CHANNEL_STATE,
    E_SETUP_VIDEO_CHANNEL_STATE,
    E_PLAY_CHANNEL_STATE,
}E_RTSP_STATE;

typedef enum{
    E_FORWARD_DISABLE = 0,
    E_FORWARD_ALLOW
    
}E_FORWARD_STATE;

typedef enum{
    E_NONE = 0,
    E_RTSP_HANDSAKEING_DONE,
    E_DIRECT_FORWARD_TO_INTERNAL,
    
}E_REMOTE_RTSP_CLIENT_STATE;

typedef struct{
    char                *rtsp_url;
    struct sockaddr_in  rtsp_dest;
    int                 local_fd;
}REMOTE_RTSP_CLIENT_BACKUP;

typedef struct{
    int                 fd;
    long                session;
    long long           timeout_ms;
    long long           timeout_timer;
    char                *sdp_ptr;
    char                *rtsp_url;
    struct sockaddr_in  rtsp_dest;
    
    long                c_seq;
    long                play_c_seq;
    long                pause_c_seq;
    long                get_parameter_c_seq;
    
    int                 local_fd;
    
    E_FORWARD_STATE             forward_state;
    E_REMOTE_RTSP_CLIENT_STATE  state;
}REMOTE_RTSP_CLIENT;

typedef struct{
    int     fd;
    char    *url;
    long    c_seq;
}LOCAL_RTSP_CLIENT;





//REMOTE_RTSP_CLIENT & REMOTE_RTSP_BACKUP
void close_remote_rtsp_client(REMOTE_RTSP_CLIENT *client);
void close_remote_rtsp_client_by_index(int index);
void close_remote_rtsp_client_backup(REMOTE_RTSP_CLIENT_BACKUP *backup);
void close_remote_rtsp_client_backup_by_index(int i);
void close_all_remote_rtsp_clients();
BOOL is_active_client(REMOTE_RTSP_CLIENT *client);
BOOL is_active_backup(REMOTE_RTSP_CLIENT_BACKUP *backup);
BOOL is_active_client_by_index(int i);
BOOL is_active_backup_by_index(int i);
void backup_remote_rtsp_client(REMOTE_RTSP_CLIENT_BACKUP *backup, REMOTE_RTSP_CLIENT *client);
void restore_remote_rtsp_client(REMOTE_RTSP_CLIENT *client, REMOTE_RTSP_CLIENT_BACKUP *backup);
REMOTE_RTSP_CLIENT *get_empty_client();
REMOTE_RTSP_CLIENT *get_client_by_index(int index);
REMOTE_RTSP_CLIENT *get_client_by_local_fd(int local_fd);
REMOTE_RTSP_CLIENT_BACKUP *get_backup_by_index(int index);
int get_max_connection();
void print_remote_rtsp_client(REMOTE_RTSP_CLIENT *client);
void print_remote_rtsp_client_backup(REMOTE_RTSP_CLIENT_BACKUP *backup);

//Handler
long handleLocalRTSP(int fd, char *recv_buf, REMOTE_RTSP_CLIENT *client);
void handle_sdp(REMOTE_RTSP_CLIENT *client);
void handle_remote_rtsp_handshaking(char *recv_buf, REMOTE_RTSP_CLIENT *client);
void handle_remote_rtsp_reply(char *recv_buf, REMOTE_RTSP_CLIENT *client);
void hadle_remote_rtp_reply(char *data, long len, REMOTE_RTSP_CLIENT *client);
void handle_direct_connect_request(REMOTE_RTSP_CLIENT *client, char *recv_buf, long len);
void handle_direct_connect_reply(REMOTE_RTSP_CLIENT *client, char *recv_buf, long len);

//Othres
long rtsp_command_sender(char *COMMAND, REMOTE_RTSP_CLIENT *client);
long start_rtsp_connection_and_send_DESCRIBE_command(REMOTE_RTSP_CLIENT *client);
BOOL start_direct_rtsp_connection(char *recv_buf, REMOTE_RTSP_CLIENT *client);
BOOL get_sdp(char *recv_buf, REMOTE_RTSP_CLIENT *client);
int get_cseq_number(char *data);
BOOL parse_rtsp_url(char *source_url, struct sockaddr_in *addr, char *url);

REMOTE_RTSP_CLIENT *get_mapping_rtsp_remote_and_loca_client(char *recv_buf);


typedef struct{
    
    //REMOTE_RTSP_CLIENT
    
    //LOCAL_RTSP_CLIENT
    LOCAL_RTSP_CLIENT *(*get_local_rtsp_client)(void);
    void (*close_local_rtsp_client)(void);
    
}RTSP;

RTSP *initRTSP();

#endif /* rtsp_h */
