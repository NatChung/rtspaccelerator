//
//  unit_test.c
//  RTSPAccelerator
//
//  Created by Nat on 2016/10/2.
//  Copyright © 2016年 Nat. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>
#include <assert.h>
#include <arpa/inet.h>

#include "rtsp.h"
#include "MainServer.h"
#include "socket.h"

RTSP *theRTSP = NULL;

//MainServer.c
void test_init_remote_rtsp_clients(){
    
    assert(get_max_connection() == 256);
    
    int i=0;
    for(i=0;i<get_max_connection();i++){
        assert(get_client_by_index(i)->fd == 0);
        assert(get_client_by_index(i)->session == 0);
        assert(get_client_by_index(i)->timeout_ms == 0);
        assert(get_client_by_index(i)->timeout_timer == 0);
        assert(get_client_by_index(i)->c_seq == 0);
        assert(get_client_by_index(i)->sdp_ptr == NULL);
        assert(get_client_by_index(i)->rtsp_url == NULL);
        assert(get_client_by_index(i)->rtsp_dest.sin_addr.s_addr == 0);
        assert(get_client_by_index(i)->rtsp_dest.sin_port == 0);
    }
}


void test_close_remote_rtsp_client(){
    REMOTE_RTSP_CLIENT client;
    client.fd = open_udp_server_socket(0);
    client.sdp_ptr = (char *) malloc(10);
    client.rtsp_url = (char *) malloc(20);
    close_remote_rtsp_client(&client);
    
    assert(client.fd == 0);
    assert(client.session == 0);
    assert(client.timeout_ms == 0);
    assert(client.timeout_timer == 0);
    assert(client.c_seq == 0);
    assert(client.sdp_ptr == NULL);
    assert(client.rtsp_url == NULL);
    assert(client.rtsp_dest.sin_addr.s_addr == 0);
    assert(client.rtsp_dest.sin_port == 0);
}

extern BOOL parse_rtsp_url(char *source_url, struct sockaddr_in *addr, char *url);
void test_parse_rtsp_url(){
    char *source_rul = "connect:rtsp://60.60.60.11:1935/live/leady-streamA";
    struct sockaddr_in dest;
    char target_url[SHORT_STRING_LEN];
    
    assert(parse_rtsp_url(source_rul, &dest, target_url) == YES);
    assert(dest.sin_addr.s_addr == inet_addr("60.60.60.11"));
    assert(dest.sin_port == htons(1935));
    assert(strcmp(target_url, "live/leady-streamA") == 0);
    
    source_rul = "close:rtsp://127.0.0.1/live/leady-streamG";
    assert(parse_rtsp_url(source_rul, &dest, target_url) == YES);
    assert(dest.sin_addr.s_addr == inet_addr("127.0.0.1"));
    assert(dest.sin_port == htons(554));
    assert(strcmp(target_url, "live/leady-streamG") == 0);
    
}

void test_get_sdp(){
    char *package = "RTSP/1.0 200 OK\r\nCSeq: 3\r\nServer: Wowza Streaming Engine 4.5.0 build18676\r\nCache-Control: no-cache\r\nExpires: Sun, 2 Oct 2016 12:04:58 CST\r\nContent-Length: 593\r\nContent-Base: rtsp://60.251.139.247/live/leday-streamA_720p/\r\nDate: Sun, 2 Oct 2016 12:04:58 CST\r\nContent-Type: application/sdp\r\nSession: 728103566;timeout=60\r\n\r\nv=0\r\no=- 728103566 728103566 IN IP4 127.0.0.1\r\ns=leday-streamA_720p\r\nc=IN IP4 0.0.0.0\r\nt=0 0\r\na=sdplang:en\r\na=range:npt=now-\r\na=control:*\r\nm=audio 0 RTP/AVP 96\r\na=rtpmap:96 mpeg4-generic/44100/2\r\na=fmtp:96 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1210\r\na=control:trackID=1\r\nm=video 0 RTP/AVP 97\r\na=rtpmap:97 H264/90000\r\na=fmtp:97 packetization-mode=1;profile-level-id=4D4020;sprop-parameter-sets=Z01AIJZSAKALdgKhAAADAAEAAAMAPOJgAK6MAAFdHz8Y4O0LFyQ=,aOuNSA==\r\na=cliprect:0,0,720,1280\r\na=framesize:97 1280-720\r\na=framerate:30.0\r\na=control:trackID=2\r\n";
    
    REMOTE_RTSP_CLIENT client;
    memset(&client, 0, sizeof(REMOTE_RTSP_CLIENT));
    get_sdp(package, &client);
    
    assert(client.session == 728103566);
    assert(client.timeout_ms == (60000*0.7));
    assert(strcmp(client.sdp_ptr, "v=0\r\no=- 728103566 728103566 IN IP4 127.0.0.1\r\ns=leday-streamA_720p\r\nc=IN IP4 0.0.0.0\r\nt=0 0\r\na=sdplang:en\r\na=range:npt=now-\r\na=control:*\r\nm=audio 0 RTP/AVP 96\r\na=rtpmap:96 mpeg4-generic/44100/2\r\na=fmtp:96 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1210\r\na=control:trackID=1\r\nm=video 0 RTP/AVP 97\r\na=rtpmap:97 H264/90000\r\na=fmtp:97 packetization-mode=1;profile-level-id=4D4020;sprop-parameter-sets=Z01AIJZSAKALdgKhAAADAAEAAAMAPOJgAK6MAAFdHz8Y4O0LFyQ=,aOuNSA==\r\na=cliprect:0,0,720,1280\r\na=framesize:97 1280-720\r\na=framerate:30.0\r\na=control:trackID=2\r\n")==0);
}

void test_get_empty_client(){
    
    int i=0;
    close_all_remote_rtsp_clients();
    
    for(i=0;i<3;i++){
        get_client_by_index(i)->fd = i;
        get_client_by_index(i)->rtsp_url = "1232";
        get_client_by_index(i)->sdp_ptr = "jfkdjfk";
        get_client_by_index(i)->rtsp_dest.sin_addr.s_addr = i;
        get_client_by_index(i)->rtsp_dest.sin_port = i;
    }
    
    REMOTE_RTSP_CLIENT *found_cient = get_empty_client();
    assert(found_cient == get_client_by_index(3));
}

extern BOOL parse_rtsp_request(char *source_url, struct sockaddr_in *addr, char *url);
void test_parse_rtsp_request(){
    char *package = "OPTION rtsp://127.0.0.1:1935/rtsp://60.251.139.247:554/live/leday-streamA_720p RTSP/1.0\r\nCSeq: 3\r\nAccept: application/sdp\r\n\r\n";
    struct sockaddr_in dest;
    char target_url[SHORT_STRING_LEN];
    
    assert(parse_rtsp_request(package, &dest, target_url) == YES);
    assert(dest.sin_addr.s_addr == inet_addr("60.251.139.247"));
    assert(dest.sin_port == htons(554));
    assert(strcmp(target_url, "live/leday-streamA_720p") == 0);
    
    
    package = "OPTION rtsp://60.251.139.247:554/live/leday-streamA_720p RTSP/1.0\r\nCSeq: 3\r\nAccept: application/sdp\r\n\r\n";
    assert(parse_rtsp_request(package, &dest, target_url) == YES);
    assert(dest.sin_addr.s_addr == inet_addr("60.251.139.247"));
    assert(dest.sin_port == htons(554));
    assert(strcmp(target_url, "live/leday-streamA_720p") == 0);
}

extern BOOL assign_to_fd_array(int *array, const int count, int fd);
void test_assign_to_fd_array(){
    int array[5];
    memset(array, 0x1f, sizeof(int)*5);
    array[3] = 0;
    assert(assign_to_fd_array(array, 5, 1)==YES);
    assert(array[3]==1);
}


void test_backup_remote_rtsp_client(){
    
    char *test_url = "live/leday-streamA_720p";
    REMOTE_RTSP_CLIENT client;
    client.local_fd = 3;
    client.rtsp_url = test_url;
    client.rtsp_dest.sin_family = AF_INET;
    client.rtsp_dest.sin_addr.s_addr = inet_addr("12.23.32.12");
    client.rtsp_dest.sin_port = ntohs(1234);
    
    REMOTE_RTSP_CLIENT_BACKUP backup;
    backup_remote_rtsp_client(&backup, &client);
    assert(backup.local_fd == 3);
    assert(strcmp(backup.rtsp_url, client.rtsp_url)==0);
    assert(backup.rtsp_dest.sin_family == AF_INET);
    assert(backup.rtsp_dest.sin_addr.s_addr == inet_addr("12.23.32.12"));
    assert(backup.rtsp_dest.sin_port == htons(1234));
}

void test_restore_remote_rtsp_client(){
    
    char *test_url = "live/leday-streamA_720p";
    REMOTE_RTSP_CLIENT_BACKUP backup;
    backup.local_fd = 3;
    backup.rtsp_url = test_url;
    backup.rtsp_dest.sin_family = AF_INET;
    backup.rtsp_dest.sin_addr.s_addr = inet_addr("12.23.32.12");
    backup.rtsp_dest.sin_port = ntohs(1234);
    
    REMOTE_RTSP_CLIENT client;
    memset(&client, 0, sizeof(REMOTE_RTSP_CLIENT));
    restore_remote_rtsp_client(&client, &backup);
    
    assert(client.local_fd == 3);
    assert(strcmp(client.rtsp_url, backup.rtsp_url)==0);
    assert(client.rtsp_dest.sin_family == AF_INET);
    assert(client.rtsp_dest.sin_addr.s_addr == inet_addr("12.23.32.12"));
    assert(client.rtsp_dest.sin_port == htons(1234));
    
}

void unit_test(){
    
    theRTSP = initRTSP();
    
    test_init_remote_rtsp_clients();
    test_close_remote_rtsp_client();
    test_parse_rtsp_url();
    test_get_empty_client();
    test_get_sdp();
    test_parse_rtsp_request();
    test_assign_to_fd_array();
    test_backup_remote_rtsp_client();
    test_restore_remote_rtsp_client();
    
    free(theRTSP);
}
