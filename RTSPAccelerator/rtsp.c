//
//  rtsp.c
//  R2HServer
//
//  Created by Nat on 2016/9/29.
//
//

#include "rtsp.h"
#include "socket.h"

#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>

#define MAX_CONNECTION  256


static REMOTE_RTSP_CLIENT_BACKUP gRemoteRtspClientBackup[MAX_CONNECTION];
static REMOTE_RTSP_CLIENT gRemoRtspClient[MAX_CONNECTION];

const char *target_host = "60.251.139.247";
const unsigned short CHANNEL_PORT = 554;

//extern const char *target_host;
//extern const unsigned short CHANNEL_PORT;

int get_cseq_number(char *data)
{
    return get_number_from_string(data, "CSeq: ", "\r\n");
}

static int get_content_length(char *data )
{
    return get_number_from_string(data, "Content-Length:", "\r\n");
}

static int get_session_number(char *data )
{
    return get_number_from_string(data, "Session: ", ";");
}

static int get_session_timeout_number(char *data)
{
    return get_number_from_string(data, "timeout=", "\r\n");
}

static long get_rtsp_header_length(char *data)
{
    char *end_str = strstr(data, "\r\n\r\n");
    if(end_str){
        return end_str +4 - data;
    }
    
    return 0;
}

#pragma mark - OTHERS

char *generate_rtsp_request_command(char *COMMAND, REMOTE_RTSP_CLIENT *client) {
    static char recv_buf[SHORT_STRING_LEN];
    sprintf(recv_buf, "%s rtsp://%s:%d/%s RTSP/1.0\r\nCSeq: %ld\r\nSession: %ld\r\nRange: npt=0.000-\r\n\r\n", COMMAND, inet_ntoa(client->rtsp_dest.sin_addr), ntohs(client->rtsp_dest.sin_port), client->rtsp_url, client->c_seq, client->session);
    return recv_buf;
}

char *generate_rtsp_option_request_command(REMOTE_RTSP_CLIENT *client) {
    static char recv_buf[SHORT_STRING_LEN];
    sprintf(recv_buf, "%s rtsp://%s:%d/%s RTSP/1.0\r\nCSeq: %ld\r\n", OPTIONS, inet_ntoa(client->rtsp_dest.sin_addr), ntohs(client->rtsp_dest.sin_port), client->rtsp_url, client->c_seq);
    return recv_buf;
}

char *generate_rtsp_describe_request_command(REMOTE_RTSP_CLIENT *client) {
    static char recv_buf[SHORT_STRING_LEN];
    sprintf(recv_buf, "%s rtsp://%s:%d/%s RTSP/1.0\r\nCSeq: %ld\r\nAccept: application/sdp\r\n\r\n", DESCRIBE, inet_ntoa(client->rtsp_dest.sin_addr), ntohs(client->rtsp_dest.sin_port), client->rtsp_url, client->c_seq);
    return recv_buf;
}

static void get_transport_str(char *recv_buf, char* transport)
{
    char *start = strstr(recv_buf, "Transport:");
    char *end = strstr(start, "\r\n");
    memset(transport, 0, SHORT_STRING_LEN);
    memcpy(transport, start, end - start);
}

long rtsp_command_sender(char *COMMAND, REMOTE_RTSP_CLIENT *client){
    
    print_log("\n SEDN %s to %s:%ld/%s", COMMAND, inet_ntoa(client->rtsp_dest.sin_addr), ntohs(client->rtsp_dest.sin_port), client->rtsp_url);
    
    char *send_buf = generate_rtsp_request_command(COMMAND, client);
    long ret = send(client->fd, send_buf, strlen(send_buf), 0);
    if(ret <= 0){
        close_remote_rtsp_client(client);
        return ret;
    }
    return ret;
}

static void get_session_and_timeout(char *recv_buf, REMOTE_RTSP_CLIENT *client)
{
    client->session = get_session_number(recv_buf);
    client->timeout_ms = get_session_timeout_number(recv_buf) * 1000 * 0.7;
    if(client->timeout_ms > 0){
        client->timeout_timer = gettime_ms2() + client->timeout_ms ;
    }
}

void hadle_remote_rtp_reply(char *data, long len, REMOTE_RTSP_CLIENT *client)
{
    if(client->forward_state != E_FORWARD_DISABLE && client->local_fd > 0){
        long ret = send(client->local_fd, data, len, 0);
        if(ret < 0){
            close_remote_rtsp_client(client);
        }
    }
}

BOOL parse_rtsp_url(char *source_url, struct sockaddr_in *addr, char *url){
    char ip_str[SHORT_STRING_LEN], port_str[SHORT_STRING_LEN];
    char *start_ptr, *end_ptr, *port_start, *port_end, *url_start;
    
    //Set to defaut port number of RTSP, 554
    addr->sin_family = AF_INET;
    addr->sin_port = htons(554);
    
    start_ptr = strstr(source_url, "rtsp://") + strlen("rtsp://");
    if(start_ptr != NULL){
        if(strstr(start_ptr, ":")){
            end_ptr = strstr(start_ptr, ":");
            port_start = end_ptr +1;
            port_end = strstr(port_start, "/");
            url_start = port_end +1;
            
            memset(port_str, 0, SHORT_STRING_LEN);
            memcpy(port_str, port_start, port_end - port_start);
            addr->sin_port = htons(atoi(port_str));
            
        }else {
            end_ptr = strstr(start_ptr, "/");
            url_start = end_ptr +1;
        }
        
        memset(ip_str, 0, SHORT_STRING_LEN);
        memcpy(ip_str, start_ptr, end_ptr-start_ptr);
        addr->sin_addr.s_addr = inet_addr(ip_str);
        strcpy(url, url_start);
        return YES;
    }
    
    
    return NO;
}



BOOL parse_rtsp_request(char *source_url, struct sockaddr_in *addr, char *url){
    
    
    char ip_str[SHORT_STRING_LEN], port_str[SHORT_STRING_LEN], _url[SHORT_STRING_LEN];
    char *start_ptr, *end_ptr, *port_start, *port_end, *url_start, *url_end;
    
    //Set to defaut port number of RTSP, 554
    addr->sin_family = AF_INET;
    addr->sin_port = htons(554);
    
    start_ptr = strstr(source_url, "rtsp://") + strlen("rtsp://");
    if(start_ptr != NULL){
        if(strstr(start_ptr, ":")){
            end_ptr = strstr(start_ptr, ":");
            port_start = end_ptr +1;
            port_end = strstr(port_start, "/");
            url_start = port_end +1;
            
            memset(port_str, 0, SHORT_STRING_LEN);
            memcpy(port_str, port_start, port_end - port_start);
            addr->sin_port = htons(atoi(port_str));
            
        }else {
            end_ptr = strstr(start_ptr, "/");
            url_start = end_ptr +1;
        }
        
        memset(ip_str, 0, SHORT_STRING_LEN);
        memcpy(ip_str, start_ptr, end_ptr-start_ptr);
        addr->sin_addr.s_addr = inet_addr(ip_str);
        
        url_end = strstr(url_start, " RTSP/1.0\r\n");
        if(url_end !=NULL){
            memset(_url, 0, SHORT_STRING_LEN);
            memcpy(_url, url_start, url_end-url_start);
        }
        
        if(strncmp(_url, "rtsp://", strlen("rtsp://"))==0){
            parse_rtsp_url(_url, addr, url);
        }else{
            strcpy(url, _url);
        }
        
        
        
        return YES;
    }
    
    
    return NO;
}

BOOL start_direct_rtsp_connection(char *recv_buf, REMOTE_RTSP_CLIENT *client){
    
    struct sockaddr_in addr;
    char url[SHORT_STRING_LEN];
    parse_rtsp_request(recv_buf, &addr, url);
    
    memcpy(&client->rtsp_dest, &addr, sizeof(struct sockaddr_in));
    client->rtsp_url = (char *)malloc(strlen(url)+1);
    strcpy(client->rtsp_url, url);
    
    client->fd = start_connect_to_tcp_server(client->rtsp_dest);
    if (client->fd<0){
        print_err( "\n start_connect_to_tcp_server error");
        close_remote_rtsp_client(client);
        return NO;
    }
    
    client->state = E_DIRECT_FORWARD_TO_INTERNAL;
    
    return YES;
}

long start_rtsp_connection_and_send_DESCRIBE_command(REMOTE_RTSP_CLIENT *client)
{
    client->fd = start_connect_to_tcp_server(client->rtsp_dest);
    if (client->fd<0){
        print_err( "\n start_connect_to_tcp_server error");
        close_remote_rtsp_client(client);
        return -1;
    }

    client->c_seq = E_DESCRIBE_STATE;
    char *send_buf = generate_rtsp_describe_request_command(client);
    long ret = send(client->fd, send_buf, strlen(send_buf), 0);
    if(ret<0){
        print_err("\n send DESCRIBE to RTSP Server error ret = %ld, close rtsp client", ret);
        close_remote_rtsp_client(client);
        return -1;
    }
    
    return 0;
}

BOOL get_sdp(char *recv_buf, REMOTE_RTSP_CLIENT *client)
{
    long sdp_length = 0;
    long recv_buf_len = strlen(recv_buf);
    if(client->sdp_ptr==NULL) {
        get_session_and_timeout(recv_buf, client);
        
        sdp_length  = get_content_length(recv_buf);
        if(sdp_length<=0){
            print_err("\n get SDP length error!");
            close_remote_rtsp_client(client);
            return NO;
        }
        
        client->sdp_ptr = (char *)malloc(sdp_length +1);
        client->sdp_ptr[sdp_length] = '\0';
        long rtsp_header_len = get_rtsp_header_length(recv_buf);
        if(recv_buf_len > rtsp_header_len){
            long payload_len = recv_buf_len - rtsp_header_len;
            memcpy(client->sdp_ptr, recv_buf +rtsp_header_len, payload_len);
        }else{
            assert("recv len < rtsp header length");
            return NO;
        }
        
    }
    
    return YES;
}

REMOTE_RTSP_CLIENT *get_mapping_rtsp_remote_and_loca_client(char *recv_buf){
    
    int i=0;
    struct sockaddr_in addr;
    char url[SHORT_STRING_LEN];
    char original_url[SHORT_STRING_LEN];
    
    parse_original_url(recv_buf, original_url);
    parse_rtsp_request(recv_buf, &addr, url);
    
    for(i=0;i<MAX_CONNECTION;i++){
        if(gRemoRtspClient[i].fd > 0  &&
           gRemoRtspClient[i].rtsp_dest.sin_addr.s_addr == addr.sin_addr.s_addr &&
           gRemoRtspClient[i].rtsp_dest.sin_port == addr.sin_port &&
           strcmp(gRemoRtspClient[i].rtsp_url, url)==0){
            return &(gRemoRtspClient[i]);
        }
    }
    
    
    
    return NULL;
}

#pragma mark - RTSPLocalClient

LOCAL_RTSP_CLIENT aLocalClient;
static void init_local_rtsp_client()
{
    memset(&aLocalClient, 0, sizeof(LOCAL_RTSP_CLIENT));
}

static LOCAL_RTSP_CLIENT *get_local_rtsp_client()
{
    return &aLocalClient;
}

static void close_local_rtsp_client()
{
    if(aLocalClient.fd > 0){
        close(aLocalClient.fd);
    }
    
    if(aLocalClient.url != NULL){
        free(aLocalClient.url);
    }
    
    memset(&aLocalClient, 0, sizeof(LOCAL_RTSP_CLIENT));
}


#pragma mark - RTSPRemoteClien

static void init_remote_rtsp_clients(){
    memset(&gRemoRtspClient, 0, MAX_CONNECTION*sizeof(REMOTE_RTSP_CLIENT));
}

static void init_remote_rtsp_client_backup(){
    memset(&gRemoteRtspClientBackup, 0, MAX_CONNECTION*sizeof(REMOTE_RTSP_CLIENT_BACKUP));
}

void close_all_remote_rtsp_clients(){
    int i=MAX_CONNECTION;
    for(i=0;i<MAX_CONNECTION;i++){
        REMOTE_RTSP_CLIENT *client = get_client_by_index(i);
        close_remote_rtsp_client(client);
    }
}

BOOL is_active_client(REMOTE_RTSP_CLIENT *client){
    return (client->fd > 0 &&
            client->rtsp_url != NULL &&
            client->rtsp_dest.sin_addr.s_addr != 0 &&
            client->rtsp_dest.sin_port != 0);
}

BOOL is_active_backup(REMOTE_RTSP_CLIENT_BACKUP *backup){
    return (backup->rtsp_dest.sin_family!=0 &&
            backup->rtsp_dest.sin_addr.s_addr != 0 &&
            backup->rtsp_dest.sin_port != 0 &&
            backup->rtsp_url != NULL &&
            strlen(backup->rtsp_url)>0);
}

BOOL is_active_client_by_index(int i){
    REMOTE_RTSP_CLIENT *client = get_client_by_index(i);
    return is_active_client(client);
}

BOOL is_active_backup_by_index(int i){
    REMOTE_RTSP_CLIENT_BACKUP *backup = get_backup_by_index(i);
    return is_active_backup(backup);
}

void backup_remote_rtsp_client(REMOTE_RTSP_CLIENT_BACKUP *backup, REMOTE_RTSP_CLIENT *client){
    backup->local_fd = client->local_fd;
    memcpy(&backup->rtsp_dest, &client->rtsp_dest, sizeof(struct sockaddr_in));
    if(client->rtsp_url && strlen(client->rtsp_url)>0){
        backup->rtsp_url = (char *)malloc(strlen(client->rtsp_url) +1);
        strcpy(backup->rtsp_url, client->rtsp_url);
    }
}

void restore_remote_rtsp_client(REMOTE_RTSP_CLIENT *client, REMOTE_RTSP_CLIENT_BACKUP *backup){
    client->local_fd = backup->local_fd;
    memcpy(&client->rtsp_dest, &backup->rtsp_dest, sizeof(struct sockaddr_in));
    client->rtsp_url = (char *)malloc(strlen(backup->rtsp_url) +1);
    strcpy(client->rtsp_url, backup->rtsp_url);
}

REMOTE_RTSP_CLIENT *get_empty_client(){
    
    int i=0;
    for(i=0;i<MAX_CONNECTION;i++){
        if(gRemoRtspClient[i].fd == 0 &&
           gRemoRtspClient[i].session == 0&&
           gRemoRtspClient[i].timeout_ms == 0 &&
           gRemoRtspClient[i].timeout_timer == 0 &&
           gRemoRtspClient[i].sdp_ptr == NULL &&
           gRemoRtspClient[i].rtsp_url == 0 )
            return &(gRemoRtspClient[i]);
    }
    
    assert("Out side connection full");
    return NULL;
}

void close_remote_rtsp_client(REMOTE_RTSP_CLIENT *client){
    
    if(client->fd > 0){
        close(client->fd);
    }
    
    if(client->sdp_ptr){
        free(client->sdp_ptr);
    }
    
    if(client->rtsp_url){
        free(client->rtsp_url);
    }
    
    memset(client, 0, sizeof(REMOTE_RTSP_CLIENT));
}

void close_remote_rtsp_client_by_index(int i){
    REMOTE_RTSP_CLIENT *client = get_client_by_index(i);
    close_remote_rtsp_client(client);
}

void close_remote_rtsp_client_backup(REMOTE_RTSP_CLIENT_BACKUP *backup){
    if(backup->rtsp_url){
        free(backup->rtsp_url);
    }
    memset(backup, 0, sizeof(REMOTE_RTSP_CLIENT_BACKUP));
}

void close_remote_rtsp_client_backup_by_index(int i){
    REMOTE_RTSP_CLIENT_BACKUP *backup = get_backup_by_index(i);
    close_remote_rtsp_client_backup(backup);
}

REMOTE_RTSP_CLIENT *get_client_by_index(int index){
    if(index>= MAX_CONNECTION){
        return NULL;
    }
    return &(gRemoRtspClient[index]);
}

REMOTE_RTSP_CLIENT *get_client_by_local_fd(int local_fd){
    int i=0;
    for(i=0;i<MAX_CONNECTION;i++){
        REMOTE_RTSP_CLIENT *client = get_client_by_index(i);
        if(client->local_fd>0 && client->local_fd == local_fd)
            return client;
    }
    
    return NULL;
}

REMOTE_RTSP_CLIENT_BACKUP *get_backup_by_index(int index){
    if(index>= MAX_CONNECTION){
        return NULL;
    }
    return &(gRemoteRtspClientBackup[index]);
}

int get_max_connection(){
    return MAX_CONNECTION;
}

void print_remote_rtsp_client(REMOTE_RTSP_CLIENT *client){
    print_log("\n --------------------REMOTE_RTSP_CLIENT: ");
    print_log("\n fd: %d", client->fd);
    print_log("\n session: %ld", client->session);
    print_log("\n timeout_ms: %lld", client->timeout_ms);
    print_log("\n timeout_timer: %lld", client->timeout_timer);
    print_log("\n sdp_ptr: %s", client->sdp_ptr);
    print_log("\n rtsp_url: %s", client->rtsp_url);
    print_log("\n rtsp_dest.sin_addr: %s", inet_ntoa(client->rtsp_dest.sin_addr));
    print_log("\n rtsp_dest.sin_port: %d", ntohs(client->rtsp_dest.sin_port));
    
    print_log("\n c_seq: %ld", client->c_seq);
    print_log("\n handshaked: %d", client->state);
    print_log("\n play_c_seq: %ld", client->play_c_seq);
    print_log("\n pause_c_seq: %ld", client->pause_c_seq);
    print_log("\n get_parameter_c_seq: %ld", client->get_parameter_c_seq);
    
    print_log("\n local_fd: %d", client->local_fd);
    print_log("\n forward_state: %d", client->forward_state);
    
    print_log("\n ");
}

void print_remote_rtsp_client_backup(REMOTE_RTSP_CLIENT_BACKUP *backup){
    
    print_log("\n --------------------REMOTE_RTSP_CLIENT_BACKUP: ");
    print_log("\n rtsp_url: %s", backup->rtsp_url);
    print_log("\n rtsp_dest.sin_addr: %s", inet_ntoa(backup->rtsp_dest.sin_addr));
    print_log("\n rtsp_dest.sin_port: %d", ntohs(backup->rtsp_dest.sin_port));
    print_log("\n local_fd: %d", backup->local_fd);
    print_log("\n ");
    
}

#pragma mark - Handler

long handleLocalRTSP(int fd, char *recv_buf, REMOTE_RTSP_CLIENT *client)
{
    int c_seq = get_cseq_number(recv_buf);
    if(strncmp(recv_buf, OPTIONS, strlen(OPTIONS))==0){
        sprintf(recv_buf, "RTSP/1.0 200 OK\r\nCSeq: %d\r\nCache-Control: no-cache\r\nPublic: DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE, OPTIONS, ANNOUNCE, RECORD, GET_PARAMETER\r\nSupported: play.basic, con.persistent\r\n\r\n", c_seq);
        
    }else if(strncmp(recv_buf, DESCRIBE, strlen(DESCRIBE))==0){
        
        sprintf(recv_buf, "RTSP/1.0 200 OK\r\nCSeq: %d\r\nCache-Control: no-cache\r\nContent-Length: %ld\r\nContent-Base: rtsp://127.0.0.1:1935/live/leday-streamA_720p/\r\nContent-Type: application/sdp\r\nSession: %ld;timeout=%lld\r\n\r\n%s\r\n", c_seq, strlen(client->sdp_ptr), client->session, client->timeout_ms/1000, client->sdp_ptr);
        
    }else if(strncmp(recv_buf, SETUP, strlen(SETUP))==0){
        char transport[SHORT_STRING_LEN];
        get_transport_str(recv_buf, transport);
        sprintf(recv_buf, "RTSP/1.0 200 OK\r\nCSeq: %d\r\nCache-Control: no-cache\r\n%s\r\nSession: %ld;timeout=%lld\r\n\r\n", c_seq, transport, client->session, client->timeout_ms/1000);
        
    }else if(strncmp(recv_buf, PLAY, strlen(PLAY))==0){
        
        sprintf(recv_buf, "RTSP/1.0 200 OK\r\nCSeq: %d\r\nCache-Control: no-cache\r\nRange: npt=now-\r\nSession: %ld;timeout=%lld\r\n\r\n", c_seq, client->session, client->timeout_ms/1000);
        
        client->forward_state = E_FORWARD_ALLOW;
        client->c_seq = client->play_c_seq;
        rtsp_command_sender(PLAY, client);
        
    }else if(strncmp(recv_buf, TEARDOWN, strlen(TEARDOWN))==0){
        
        client->local_fd = -1;
        client->forward_state = E_FORWARD_DISABLE;
        print_err("\n Got TEARDOWN");
        
        client->c_seq = client->pause_c_seq;
        rtsp_command_sender(PAUSE, client);
        return -1;
        
    }else if(strncmp(recv_buf, GET_PARAMETER, strlen(GET_PARAMETER))==0){
        
        sprintf(recv_buf, "RTSP/1.0 200 OK\r\nCSeq: %d\r\nCache-Control: no-cache\r\nPublic: DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE, OPTIONS, ANNOUNCE, RECORD, GET_PARAMETER\r\nSupported: play.basic, con.persistent\r\nSession: %ld;timeout=%lld\r\n\r\n", c_seq, client->session, client->timeout_ms/1000);
    }else {
        return 1;
    }
    
    return send(fd, recv_buf, strlen(recv_buf), 0);
}

void handle_sdp(REMOTE_RTSP_CLIENT *client)
{
    long ret;
    unsigned char interleaved = 0, get_server_ip=0;
    char *track_url=NULL, *server_ip=NULL;
    
    char *sdp_ptr_backup = (char *)malloc(strlen(client->sdp_ptr)+1);
    strcpy(sdp_ptr_backup, client->sdp_ptr);
    
    char *pch = strtok (sdp_ptr_backup,"\r\n");
    char recv_buf[SENDBUF_SIZE];
    
    while (pch != NULL)
    {
        if(get_server_ip==0)
        {
            if(strstr(pch, "o="))
            {
                get_server_ip = 1;
                server_ip = strstr(pch, "IP4 ");
                server_ip += strlen("IP4 ");
            }
        }
        else
        {
            track_url = strstr(pch, "a=control:");
            if(track_url && (strstr(track_url, "*")==0))
            {
                track_url += strlen("a=control:");
                
                if(strstr(server_ip, "0.0.0.0")){
                    sprintf(recv_buf, "%s %s RTSP/1.0\r\nCSeq: %ld\r\nTransport: RTP/AVP/TCP;unicast;interleaved=%d-%d\r\n\r\n", SETUP, track_url, client->c_seq+1, interleaved, interleaved+1 );
                }else{
                    sprintf(recv_buf, "%s rtsp://%s:%d/%s/%s RTSP/1.0\r\nCSeq: %ld\r\nTransport: RTP/AVP/TCP;unicast;interleaved=%d-%d\r\n\r\n", SETUP, inet_ntoa(client->rtsp_dest.sin_addr), ntohs(client->rtsp_dest.sin_port), client->rtsp_url,track_url, client->c_seq+1, interleaved, interleaved+1 );
                }
                
                ret = send(client->fd, recv_buf, strlen(recv_buf), 0);
                if(ret<0){
                    print_err("\n send SETUP to RTSP Server error ret = %ld, close rtsp client", ret);
                    close_remote_rtsp_client(client);
                }
                
                ++client->c_seq;
                interleaved += 2;
            }
            
        }
        
        pch = strtok (NULL, "\r\n");
    }
    
    free(sdp_ptr_backup);
}

void handle_remote_rtsp_handshaking(char *recv_buf, REMOTE_RTSP_CLIENT *client)
{
    unsigned int reply_seq = get_cseq_number(recv_buf);
    if(reply_seq == E_DESCRIBE_STATE) {
        if(get_sdp(recv_buf, client)==NO){
            close_remote_rtsp_client(client);
            return;
        }
        
        handle_sdp(client);
        
    }else if(reply_seq == E_SETUP_VIDEO_CHANNEL_STATE){
        ++client->c_seq;
        rtsp_command_sender(PLAY, client);
        client->play_c_seq = client->c_seq ;
        
    }else if(reply_seq == E_PLAY_CHANNEL_STATE){
        
        print_log("\n In PLAY state, timestampe = %lld\n", gettime_ms2());
        ++client->c_seq;
        rtsp_command_sender(PAUSE, client);
        client->pause_c_seq = client->c_seq;
        client->get_parameter_c_seq = client->pause_c_seq +1;
        client->state = E_RTSP_HANDSAKEING_DONE;
        
        //print_log("Play:%ld, pause:%ld, get:%ld, curretn:%ld", client->play_c_seq, client->pause_c_seq, client->get_parameter_c_seq, client->c_seq);
    }
}

void handle_remote_rtsp_reply(char *recv_buf, REMOTE_RTSP_CLIENT *client)
{
    unsigned int reply_seq = get_cseq_number(recv_buf);
    if(client->forward_state == E_FORWARD_DISABLE && reply_seq == client->play_c_seq){
        client->c_seq = client->pause_c_seq;
        rtsp_command_sender(PAUSE, client);
    }
}

void handle_direct_connect_request(REMOTE_RTSP_CLIENT *client, char *recv_buf, long len)
{
    char *send_buf = NULL;
    client->c_seq = get_cseq_number(recv_buf);
    
    if(strncmp(recv_buf, OPTIONS, strlen(OPTIONS))==0){
        send_buf = generate_rtsp_option_request_command(client);
    }else if(strncmp(recv_buf, DESCRIBE, strlen(DESCRIBE))==0) {
        send_buf = generate_rtsp_describe_request_command(client);
    }else if(strncmp(recv_buf, SETUP, strlen(SETUP))==0){
        send_buf = generate_rtsp_request_command(SETUP, client);
    }else if(strncmp(recv_buf, PLAY, strlen(PLAY))==0){
        send_buf = generate_rtsp_request_command(PLAY, client);
    }else if(strncmp(recv_buf, TEARDOWN, strlen(TEARDOWN))==0){
        send_buf = generate_rtsp_request_command(PAUSE, client);
    }else {
        print_err("\n Recv unexpect request in direct connect- %s", recv_buf);
        send_buf = generate_rtsp_request_command(PAUSE, client);
    }
    
    long ret = send(client->fd, send_buf, strlen(send_buf), 0);
    if(ret<0){
        print_err("\n send DESCRIBE to RTSP Server error ret = %ld, close rtsp client", ret);
        close_remote_rtsp_client(client);
        return ;
    }
}

void handle_direct_connect_reply(REMOTE_RTSP_CLIENT *client, char *recv_buf, long len)
{
    if(strstr(recv_buf, "Content-Type: application/sdp")!=NULL){
        get_sdp(recv_buf, client);
    }
    
    send(client->local_fd, recv_buf, len, 0);
}

#pragma mark - RTSP Class

RTSP *initRTSP()
{
    RTSP *aRTSP = (RTSP *)malloc(sizeof(RTSP));
    
    //REMOTE_RTSP_CLIENT
    
    //LOCAL_RTSP_CLIENT
    aRTSP->get_local_rtsp_client = get_local_rtsp_client;
    aRTSP->close_local_rtsp_client = close_local_rtsp_client;
    
    //constructot:
    init_remote_rtsp_clients();
    init_remote_rtsp_client_backup();
    init_local_rtsp_client();
    
    return aRTSP;
}
