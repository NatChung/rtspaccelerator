//
//  socket.c
//  R2HServer
//
//  Created by Nat on 2016/9/29.
//
//
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include "utility.h"
#include "socket.h"

int open_tcp_server_socket(unsigned short local_port)
{
    struct sockaddr_in my_addr ;
    int tcp_fd = -1;

    tcp_fd = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP ) ;
    if (tcp_fd < 0)
    {
        print_err("open TCP socket error!\n");
        goto fail;
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_addr.s_addr = htonl (INADDR_ANY);
    my_addr.sin_port = htons(local_port);

    int on = 1;
    setsockopt(tcp_fd, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));
#if 1
    int tmp = SENDBUF_SIZE;
    if (setsockopt(tcp_fd, SOL_SOCKET, SO_SNDBUF, &tmp, sizeof(tmp)) < 0)
    {
        print_err("setsockopt error!\n");
        return -1;
    }
#endif

    /* the bind is needed to give a port to the socket now */
    if (bind(tcp_fd,(struct sockaddr *)&my_addr, sizeof(my_addr)) < 0)
    {
        print_err("TCP bind error!\n");
        goto fail;
    }

    /* listen */
    if(listen(tcp_fd, 10)<0)
    {
        print_err("TCP listen error!\n");
        goto fail;
    }
    else
    {
        //Show linsten port
        //        if(local_port==0)
        //        {
        //            int len;
        //            if(getsockname(tcp_fd, (struct sockaddr *)&my_addr, &len) == -1)
        //                perror("getsockname");
        //            print_log("\n TCP Server listening port:%d.....\n", ntohs(my_addr.sin_port));
        //        }
        //        else
        //        {
        //            print_log("\n TCP Server listening prot:%d......\n", local_port);
        //        }



    }

    return tcp_fd ;

fail:
    if (tcp_fd >= 0)
        close(tcp_fd);
    return -1 ;
}


int open_udp_server_socket(int local_port)
{
    struct sockaddr_in my_addr;
    int udp_fd = -1;
    socklen_t len = sizeof(my_addr);

    udp_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(udp_fd <0 )
    {
        print_err( "UDP open socket error!\n");
        goto fail;
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(local_port);
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    int on = 1;
    setsockopt(udp_fd, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));

    if(bind(udp_fd, (struct sockaddr *) &my_addr, sizeof(my_addr)) < 0)
    {
        print_err( "UDP bind %d error!\n", local_port);
        goto fail;
    }

    if(local_port==0)
    {
        if(getsockname(udp_fd, (struct sockaddr *)&my_addr, &len) == -1)
            perror("getsockname");
    }

    //print_log("\n UDP Server listening port:%d.....\n", ntohs(my_addr.sin_port));

    return udp_fd;

fail:
    if(udp_fd > 0)
        close(udp_fd);

    return -1;
}

void send_internal_command(char *command)
{
    struct sockaddr_in dest;
    long ret = -1;
    int connect_fd = socket(AF_INET, SOCK_DGRAM, 0);

    memset(&dest, 0, sizeof(dest));

    dest.sin_family = AF_INET;
    dest.sin_port = htons(UDP_INTERNAL_PORT);
    dest.sin_addr.s_addr =  htonl(INADDR_ANY);

    ret = sendto(connect_fd, command, strlen(command), 0, (struct sockaddr *)&dest, sizeof(dest));
    if (ret < 0){
        print_err( "send signal error");
    }

    close(connect_fd);
}

int accept_connection_from_local_player(int server_fd)
{
    socklen_t from_len;
    struct sockaddr_in  from;

    from_len = sizeof(from);
    int fd = accept(server_fd, (struct sockaddr *)&from, &from_len);
    if(fd <= 0){
        print_err( "\n [ERROR] Accept forward_rtspc_server_fd failed %s:%d\n", __FILE__, __LINE__);
        return -1;
    }
    else  //if(debugLevel & DEBUG_RTSP_FORWARD)
    {
        print_log("\n Accept forward_rtsp_clietn_fd:%d!\n", fd);
    }

    return fd;
}

int start_connect_to_tcp_server(struct sockaddr_in server_addr)
{
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd<0){
        print_err( "\n create rtsp socket error");
        return -1;
    }

    if(connect(fd, (struct sockaddr*)&server_addr, sizeof(server_addr))<0){
        print_err( "\n connect error - %s", strerror(errno));
        return -1;
    }

    return fd;
}
