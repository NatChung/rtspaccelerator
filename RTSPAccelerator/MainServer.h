//
//  R2HServer.h
//  R2HServer
//
//  Created by Chung Nat on 11/11/3.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#ifndef R2HServer_R2HServer_h
#define R2HServer_R2HServer_h

#define FRAME_BUF_SIZE  60
#define FRAME_ARRAY_CNT 6

void *socket_server(void *);

#endif
