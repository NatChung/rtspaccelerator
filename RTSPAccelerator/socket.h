//
//  socket.h
//  R2HServer
//
//  Created by Nat on 2016/9/29.
//
//

#ifndef socket_h
#define socket_h

#include <stdio.h>

#define RECVBUF_SIZE    32768
#define SENDBUF_SIZE    32768

#define UDP_INTERNAL_PORT   55433
#define TCP_INTERNAL_PORT   3333
#define RTSP_INTERNAL_PORT  1935

int open_tcp_server_socket(unsigned short local_port);
int open_udp_server_socket(int local_port);
void send_internal_command(char *command);
int accept_connection_from_local_player(int server_fd);
int start_connect_to_tcp_server(struct sockaddr_in server_addr);

#endif /* socket_h */
