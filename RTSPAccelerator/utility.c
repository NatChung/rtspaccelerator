//
//  utility.c
//  R2HServer
//
//  Created by Nat on 2016/9/29.
//
//

#include "utility.h"

#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdarg.h>


#ifdef __ANDROID__
#include <android/log.h>

#define LOG_TAG "NatDebug"
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#endif


static int netlog_fd = -1;
static struct sockaddr_in netlog_dest;

int get_number_from_string(char *data, char *keyword_start, char *keyword_end){
    char target_str[SHORT_STRING_LEN];
    char *start_str = strstr(data, keyword_start);
    if(start_str){
        start_str += strlen(keyword_start);
        char *end_str = strstr(start_str, keyword_end);
        memset(target_str, 0, SHORT_STRING_LEN);
        memcpy(target_str, start_str, end_str-start_str);
        return atoi(target_str);
    }

    return 0;
}

long long gettime_ms2()
{
    struct timeval tv;

    gettimeofday(&tv,NULL);
    return (int64_t)tv.tv_sec * 1000 + (tv.tv_usec / 1000);
}

int print_err( const char *format, ... )
{
    static char log_buf[20480];
    va_list argument_list;

    va_start(argument_list, format);
    vsprintf(log_buf, format, argument_list);

#ifndef __ANDROID__
    fprintf(stderr,"%s", log_buf);
#else
    int i=0;
    while(log_buf[i]!='\0'){
        if(log_buf[i] == '\n')
            log_buf[i] = ' ';
        ++i;
    }
    LOGE("%s", log_buf);
#endif

    if(netlog_fd>0)
    {
        sendto(netlog_fd, log_buf, strlen(log_buf), 0, (struct sockaddr *)&netlog_dest, sizeof(netlog_dest));
    }

    va_end(argument_list);

    return 0 ;
}

int print_log( const char *format, ... )
{
    static char log_buf[20480];
    va_list argument_list;

    va_start(argument_list, format);
    vsprintf(log_buf, format, argument_list);

#ifndef __ANDROID__
    fprintf(stderr, "%s", log_buf);
#else
    int i=0;
    while(log_buf[i]!='\0'){
        if(log_buf[i] == '\n')
            log_buf[i] = ' ';
        ++i;
    }
    LOGI("%s", log_buf);
#endif

    if(netlog_fd>0)
    {
        sendto(netlog_fd, log_buf, strlen(log_buf), 0, (struct sockaddr *)&netlog_dest, sizeof(netlog_dest));
    }

    va_end(argument_list);

    return 0 ;
}
