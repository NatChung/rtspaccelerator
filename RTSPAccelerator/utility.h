//
//  utility.h
//  R2HServer
//
//  Created by Nat on 2016/9/29.
//
//

#ifndef utility_h
#define utility_h

#include <stdio.h>

#define SHORT_STRING_LEN 256

typedef enum {
    NO = 0,
    YES = 1
}BOOL;

int get_number_from_string(char *data, char *keyword_start, char *keyword_end);
long long gettime_ms2(void);

int print_err( const char *format, ... );
int print_log( const char *format, ... );

#endif /* utility_h */
