//
//  R2HServer.c
//  R2HServer
//
//  Created by Chung Nat on 11/11/3.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <assert.h>

#include "MainServer.h"
#include "socket.h"
#include "utility.h"
#include "rtsp.h"

static void set_to_FD_SET(int external_rtsp_client, int *fd_max_p, fd_set *rfds_p)
{
    if (external_rtsp_client > 0){
        FD_SET(external_rtsp_client, &(*rfds_p));
        if (*fd_max_p < external_rtsp_client){
            *fd_max_p = external_rtsp_client;
        }
    }
}


void timer(long long current_timestamp, REMOTE_RTSP_CLIENT *client){
    if(client->timeout_timer>0 && current_timestamp > client->timeout_timer ){
        client->timeout_timer = current_timestamp + client->timeout_ms;
        
        if(client->forward_state == E_FORWARD_DISABLE){
            client->c_seq = client->play_c_seq;
            rtsp_command_sender(PLAY, client);
        }else {
            client->c_seq = client->pause_c_seq;
            rtsp_command_sender(GET_PARAMETER, client);
        }
    }
}

static void handle_internal_rtsp_command(char *command){
    REMOTE_RTSP_CLIENT *empty_client = get_empty_client();
    if(empty_client == NULL){
        assert("RTSP Client connection full!");
    }
    empty_client->rtsp_url = (char *)malloc(SHORT_STRING_LEN);
    parse_rtsp_url(command, &empty_client->rtsp_dest, empty_client->rtsp_url);
    start_rtsp_connection_and_send_DESCRIBE_command(empty_client);
}

static void handle_internal_close_command(char *command){
    struct sockaddr_in close_addr;
    char close_url[SHORT_STRING_LEN];
    
    parse_rtsp_url(command, &close_addr, close_url);
    int i=0;
    for(i=0;i<get_max_connection();i++){
        if(get_client_by_index(i)->fd > 0 &&
           get_client_by_index(i)->rtsp_dest.sin_addr.s_addr == close_addr.sin_addr.s_addr &&
           get_client_by_index(i)->rtsp_dest.sin_port == close_addr.sin_port &&
           strcmp(get_client_by_index(i)->rtsp_url, close_url)== 0 ){
            close_remote_rtsp_client_by_index(i);
            break;
        }
    }
}

static void handle_internal_into_background_command(){
    
    int i=0, backup_index = 0;
    for(i=0;i<get_max_connection();i++){
        if(is_active_client_by_index(i)){
            REMOTE_RTSP_CLIENT *client = get_client_by_index(i);
            REMOTE_RTSP_CLIENT_BACKUP *backup = get_backup_by_index(backup_index);
            backup_remote_rtsp_client(backup, client);
            close_remote_rtsp_client(client);
            ++backup_index;
        }
    }
}

static void handle_internal_into_foreground_command(){
    int i=0;
    
    print_log("\n handle_internal_into_foreground_command");
    for(i=0;i<get_max_connection();i++){
        if(is_active_backup_by_index(i)){
            REMOTE_RTSP_CLIENT_BACKUP *backup = get_backup_by_index(i);
            REMOTE_RTSP_CLIENT *client = get_empty_client();
            restore_remote_rtsp_client(client, backup);
            close_remote_rtsp_client_backup_by_index(i);
            start_rtsp_connection_and_send_DESCRIBE_command(client);
        }
    }
}

static void handle_internal_command(char *command) {
    
    if (strncmp(command, "connect", strlen("connect"))==0){
        handle_internal_rtsp_command(command);
    }else if(strncmp(command, "close", strlen("close"))==0){
        handle_internal_close_command(command);
    }else if(strncmp(command, "background", strlen("background"))==0){
        handle_internal_into_background_command();
    } else if(strncmp(command, "foreground", strlen("foreground"))==0){
        handle_internal_into_foreground_command();
    }
}

static BOOL get_url_from_websocket(char *recv_buf, char *url){
    char *start = strstr(recv_buf, "GET /")+strlen("GET /");
    char *end = strstr(recv_buf, " HTTP/1.1");
    if(start && end){
        memset(url, 0, SHORT_STRING_LEN);
        memcpy(url, start, end - start);
        return YES;
    }
    
    return NO;
}

BOOL assign_to_fd_array(int *array, const int count, int fd){
    int i=0;
    for(i=0;i<count;i++){
        if(array[i] <= 0){
            array[i]=fd;
            return YES;
        }
    }
    
    return NO;
}

void *socket_server(void *parameter)
{
    int fd_max = -1, i=0;
    int udp_fd = -1, internal_rtsp_server = -1, tcp_server = -1;
    int tcp_client[get_max_connection()];
    long ret = -1;
    long long current_timestamp  = 0;
    REMOTE_RTSP_CLIENT *mapped_client = NULL;
    
    struct sockaddr_in from;
    struct timeval tv;
    fd_set rfds;
    char recv_buf[SENDBUF_SIZE];
    long long connected_time = 0;
    
    RTSP *theRTSP = initRTSP();
    
    memset(tcp_client, 0, sizeof(int)*get_max_connection());
    
    udp_fd = open_udp_server_socket(UDP_INTERNAL_PORT);
    if (udp_fd<0){
        print_err("Socket udp_fd error");
        goto fail;
    }
    
    internal_rtsp_server = open_tcp_server_socket(RTSP_INTERNAL_PORT);
    if (internal_rtsp_server<0){
        print_err("Socket internal_rtsp_server error");
        goto fail;
    }
    
    tcp_server = open_tcp_server_socket(TCP_INTERNAL_PORT);
    if( tcp_server < 0){
        print_err("Socket internal tcp socket error");
        goto fail;
        
    }
    
    while (1)
    {
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        
        FD_ZERO(&rfds);
        set_to_FD_SET(udp_fd, &fd_max, &rfds);
        set_to_FD_SET(internal_rtsp_server, &fd_max, &rfds);
        set_to_FD_SET(theRTSP->get_local_rtsp_client()->fd, &fd_max, &rfds);
        set_to_FD_SET(tcp_server, &fd_max, &rfds);
        
        current_timestamp =  gettime_ms2();
        for(i=0;i<get_max_connection();i++){
            timer(current_timestamp, get_client_by_index(i));
            set_to_FD_SET(get_client_by_index(i)->fd, &fd_max, &rfds);
            set_to_FD_SET(tcp_client[i], &fd_max, &rfds);
        }
        
        if (select(fd_max +100, &rfds, NULL, NULL, &tv) > 0)
        {
            if (FD_ISSET(udp_fd, &rfds))
            {
                socklen_t from_len = sizeof(from);
                ret = recvfrom(udp_fd, recv_buf, RECVBUF_SIZE, 0, (struct sockaddr *)&from, &from_len);
                if(ret<=0){
                    print_err("\n UDP socket recv data error ret = %ld", ret);
                    goto fail;
                }
                
                recv_buf[ret]='\0';
                handle_internal_command(recv_buf);
                
                continue;//avoid execture forloop below
            }
            else if(tcp_server>0 && FD_ISSET(tcp_server, &rfds))
            {
                int fd = accept_connection_from_local_player(tcp_server);
                if(fd <= 0){
                    print_err("inter tcp client accept error");
                }else{
                    assign_to_fd_array(tcp_client, get_max_connection(), fd);
                }
                
                continue;//avoid execture forloop below
            }
            else if(internal_rtsp_server > 0 && FD_ISSET(internal_rtsp_server, &rfds))
            {
                mapped_client = NULL;
                theRTSP->get_local_rtsp_client()->fd = accept_connection_from_local_player(internal_rtsp_server);
                if(theRTSP->get_local_rtsp_client()->fd <= 0){
                    print_err("internal_rtsp_server accept error");
                }
                
                continue;//avoid execture forloop below
            }
            else if(theRTSP->get_local_rtsp_client()->fd > 0 && FD_ISSET(theRTSP->get_local_rtsp_client()->fd, &rfds))
            {
                ret = recv(theRTSP->get_local_rtsp_client()->fd, recv_buf, RECVBUF_SIZE, 0);
                if(ret <= 0){
                    print_err( "\n [ERROR] rtspc_server_fd recv error(%ld) in %s:%d", ret, __FILE__, __LINE__);
                    theRTSP->close_local_rtsp_client();
                    continue;
                }
                
                recv_buf[ret] = '\0';
                if(mapped_client == NULL ){
                    mapped_client = get_mapping_rtsp_remote_and_loca_client(recv_buf);
                    if(mapped_client == NULL){
                        mapped_client = get_empty_client();
                        start_direct_rtsp_connection(recv_buf, mapped_client);
                    }
                    mapped_client->local_fd = theRTSP->get_local_rtsp_client()->fd;
                }
                
                
                if(mapped_client->state == E_DIRECT_FORWARD_TO_INTERNAL){
                    handle_direct_connect_request(mapped_client, recv_buf, ret);
                }else if (handleLocalRTSP(theRTSP->get_local_rtsp_client()->fd, recv_buf, mapped_client) <= 0){
                    close(theRTSP->get_local_rtsp_client()->fd);
                    theRTSP->get_local_rtsp_client()->fd = -1;
                    mapped_client = NULL;
                    print_err("\n close internale_rtsp");
                }
                
                
                continue;//avoid execture forloop below
            }
            
            
            for(i=0;i<get_max_connection();i++){
                
                if(get_client_by_index(i)->fd>0 && FD_ISSET(get_client_by_index(i)->fd, &rfds))
                {
                    ret = recv(get_client_by_index(i)->fd, recv_buf, RECVBUF_SIZE, 0);
                    if(ret<= 0){
                        print_err("\n RTSP socket recv data error ret = %ld, timestamp = %lld, time_ms = %lld\n", ret, gettime_ms2(), gettime_ms2() - connected_time);
                        close_remote_rtsp_client_by_index(i);
                        continue;
                    }
                    
                    recv_buf[ret] = '\0';
                    if(get_client_by_index(i)->state == E_RTSP_HANDSAKEING_DONE){
                        if(strncmp(recv_buf, "RTSP", strlen("RTSP"))==0){
                            handle_remote_rtsp_reply(recv_buf, get_client_by_index(i));
                        }else {
                            hadle_remote_rtp_reply(recv_buf, ret,  get_client_by_index(i));
                        }
                        
                    }else if(get_client_by_index(i)->state == E_DIRECT_FORWARD_TO_INTERNAL){
                        handle_direct_connect_reply(mapped_client, recv_buf, ret);
                    }else{
                        handle_remote_rtsp_handshaking(recv_buf, get_client_by_index(i));
                    }
                    
                }
                else if(tcp_client[i]>0 && FD_ISSET(tcp_client[i], &rfds))
                {
                    char url[SHORT_STRING_LEN];
                    ret = recv(tcp_client[i], recv_buf, RECVBUF_SIZE, 0);
                    if(ret>0 && get_url_from_websocket(recv_buf, url)){
                        send_internal_command(url);
                    }
                    
                    close(tcp_client[i]);
                    tcp_client[i] = -1;
                    continue;
                }
            }
        }
    }
    
    
fail:
    close(udp_fd);
    close(internal_rtsp_server);
    close(theRTSP->get_local_rtsp_client()->fd);
    close_all_remote_rtsp_clients();
    
    return NULL;
}
